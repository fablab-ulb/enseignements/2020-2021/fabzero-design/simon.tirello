# 2. Conception assistée par ordinateur

Durant la seconde semaine, le travail s’est orienté autour de l’objet choisi et la façon dont nous allions le représenter numériquement. 
L’objet que j’ai choisi, la Chaise **Nastro** Présente la particularité d’être composé de courbures que je dirai « Mécanique » car faite par une main réelle sur un papier tout ce qu’il y a de plus léger. 

La difficulté de la manœuvre est donc de réussir via Fusion360 à représenter ladite chaise la plus réaliste possible, et ce de façon le plus mécanique possible. 

## Essai n°1 - Echec

Dans un premier temps j’ai alors essayé d’utiliser le logiciel Fusion360 assez naïvement comme appris par le passé, en **extrudant** et rajoutant des éléments afin d’arriver à un semblant de forme du dossier bien particulier de la chaise. 

![](../images/formnaze.jpg) 

Autant dire que c’était moche, raté et que j’ai cru abandonner la chaise pour trouver un objet tout ce qu’il y a de plus carré. 

## Essaie n°2 – Tentative de « Form ». 


Dans un second temps je me suis alors penché sur un outil présent dans Fusion360 que je ne connaissais absolument pas, l’outil **Form*. 
Ce que je cherchais pour faire ce dossier bien particulier était un mode « bac à sable », une fonction me permettant de travailler de façon presque « triangulé » et de pouvoir abaisser des points, en étirer certains, tout en modelant ceux aux alentours. 
L’avantage de Form est que cette fonction permet de faire cela. ( A la condition que la « forme » en elle-même soit initialement produite par l’outil « form ». ) 

![](../images/formf.jpg) 

Après quelques heures passées à étirer des points et tordre la forme dans tous les sens, le résultat est déjà nettement mieux et plus encourageant que la première faite en amont. 

Suite à la modélisation du dossier, les pieds furent également modélisés mais de façon moins ‘’importante’’ (l’objet essentiel et délicat dans la conception de cette chaise étant son dossier) 

Une fois cette dernière modélisée, elle fut alors envoyée en impression, impression lancée jeudi soir presque 18h ,  attendant donc actuellement gentiment aux Casernes et impression dont j’ai hâte de voir le résultat.


# Tutoriel - Utilisation de form. 

Même si à mes yeux je ne connais encore que 0,01% de la petite partie **Form** Du logiciel, je vais tenter ici d’apposer les quelques petits fragments que j’ai pu comprendre…


## **1. Création de la forme** 


Dans un premier temps, il s’agit de créer la forme dans Form. Cette dernière pour être modifiée par l’outil nécessite impérativement d’être créer par ce dernier. 

![](../images/form1.gif) 

Ici dans l’exemple, la forme crée est un cylindre comme pour celle que j’avais crée pour la chaise.

Dans la dernière partie de la vidéo, on voit l’utilisation de la fonction **Modify**, fonction essentielle pour la modification des objets. 


## **2. Modification de la forme** 


![](../images/form3_.jpg)

Comme on le voit dans cette photo, ici nous sommes dans le panneau de modification **Modify** Permettant d’appliquer des forces mécaniques ponctuelles ou non sur l’objet. Ce panneau est composé de plusieurs lignes nous permettant de jouer de façon isolée sur la modification de l’objet. 

- En haut à gauche du rectangle rouge, on retrouve la fonction de modification. La première est la seule que j’ai utilisé pour mon objet, reprenant toutes les autres et étant à mes yeux la plus complète (du moins pour mon objet.) 

- En dessous, en **1** On retrouve l’axe, pour ce premier il est parallèle à l’axe du dessin et reprend les dimensions utilisés jusqu’alors. 

- En **2**, on retrouve un axe étant la représentation de la caméra. Je n’ai pas utilisé cet axe car à mes yeux il est imprécis. L’écran fixe ne nous permet pas de voir en 3D et même si l’on semble bien modifier l’objet, une fois à tourner sur lui-même on se rendra compte que le résultat est proche du n’importe quoi. 

- En **3**, on retrouve un outil fort utile une fois que des torsions ont été effectuées sur l’objet, car l’axe va venir se poser perpendiculairement sur la face sélectionnée, faisant abstraction des axes de création utilisés lors de la conception standard. 

- En **4** un axe que je n’ai pas essayé jusque-là. 
En dessous du rectangle rouge, la modification des paramètres jouera sur l’objet que l’on souhaite modifier, que ce soit une arrête, une face, etc… 


## **3. Modification de la trame**


Une fonction assez importante dans les outils bac à sable consiste à « subdiviser » les faces afin de pouvoir adoucir les courbes ou même rajouter des courbes à ces dernières. 

![](../images/form.jpg)

Pour ce fait, il suffit de cliquer sur des faces (**maj** enclenché pour en sélectionner plusieurs comme d’habitude ) et de clique droit/**Subdivise*.


## **4. Modification de l’épaisseur**


Pour finir ce micro-tuto, la chose importante qu’il me fallait régler avant de lancer une impression était l’épaisseur du dessin. Vu que ce dernier est réalisé entièrement dans l’outil **Form** De Fusion, il était impossible une fois sorti de l’outil de toucher à son épaisseur comme on pourrait le faire avec un Sketch classique. 

Pour cela j’ai donc du chercher et tester à peu près TOUS les outils présents dans Form, jusqu’à tomber sur le bon : **Thicken** 

![](../images/form2_.jpg)

Une fois le clic droit effectué sur l’ensemble et l’outil sélectionné, une fenêtre viendra alors s’ouvrir demandant alors l’épaisseur voulue pour créer cette dernière (avec le choix évidemment de l’intérieur/extérieur vis-à-vis des traits du dessin) et le tour est joué ! 

J’en suis actuellement Ici au niveau de l’outil **Form**… Ce tutoriel sera (je l’espère) amélioré au fur et à mesure des découvertes faites sur ce dernier. 

