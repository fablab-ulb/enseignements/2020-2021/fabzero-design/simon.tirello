# 5. Usinage assisté par ordinateur

## Formation Shaper origin.


Le 17/11/2020 nous avons eu l’occasion d’acquérir une formation pour la Shaper Origin mise à notre disposition. 
C’est une machine de **fraisage CNC guidée à la main.** 


![](docs/images/shaper.jpg) 

# 1. Connaissance de la machine. 

Dans un premier temps et au préalable de la formation, il nous a été plus que vivement conseillé de prendre connaissance des bases de la machine. Pour ce fait le site de la marque met à notre disposition toute une série de vidéos. 

Parmi ces vidéos, les deux premières renseignent assez bien sur l’utilisation de la machine, et permettent déjà de se lancer dans des premières découpes à la fin de ces deux dernières. 

- [Vidéo de présentation : Déballage](https://www.youtube.com/watch?v=Uu5kVJtEwXM)
- [Leçon sur le AirCut](https://www.youtube.com/watch?v=sdsMOPZFMRQ)
- [Site de Shaper](https://www.shapertools.com/)

Ici des vues de la machine afin de mieux mémoriser les points importants qui seront necessaire pour la compréhension de cette dernière. 

![](docs/images/shaper2.jpg) 

# 2. Mise en pratique & Formation. 

# Préalable. 

À la suite des vidéos et une présentation globale de la machine en elle-même nous sommes donc passés à la formation pratique. 

La Shaper se repère dans l’espace par une série de **Dominos** et nécessite de constamment en avoir dans son champ de vision ( La de la Shaper de situe derrière elle, au-dessus de sa poignée de transport. ) . 

Pour ce fait il est donc impératif de placer des dominos de façon régulière, de préférence des bandes de **dominos entiers** (pas de gaspillage!) espacés de 10cms les unes des autres. Les bandes ne doivent donc pas être sous l’objet à découper mais devant.

Autre chose à prendre en compte avant d’aller la machine, c’est le choix de la fraise employée. Il en existe plusieurs dont 3 & 6mm. Mais ce qu’il faut savoir avant de commencer à percer c’est la règle qui lie les fraises à la profondeur de percement. 

En effet, pour une fraise de 3mm, il ne sera possible en un passage seulement de fraiser 3mm de profondeur. **C’est un rapport tant pour tant entre fraise et profondeur.** Pour aller à 1cm de profondeur il faudra alors 4passages. 

À la suite de cela il faudra bien **fixer la planche à découper sur le plan de travail**, de préférence avec du tape double face, cela afin que les pièces ne sautent pas une fois les découpes finies, et également afin de nous protéger. 

Et enfin, **bien brancher la machine à l’aspirateur.** (et l’allumer, évidemment.) 

# Pratique. 

Il s’agit maintenant de descendre la fraise lorsqu’une figure est en surbrillance sur l’écran et de suivre la trajectoire de cette dernière avec les deux poignées de part et d’autre de la Shaper. Une fois un tracé finalisé, il est indispensable de relever la fraise avant de passer à une autre, afin de ne pas abimer cette dernière ou même les pièces réalisées. 

![](docs/images/shaper3.jpg) 

Pour cette partie il est préférable de suivre la [vidéo n°2](https://www.youtube.com/watch?v=sdsMOPZFMRQ) , reprenant point par point les réglages à établir sur l’écran. 

Bon amusement ! 

-> NB : A l’image du tuto sur l’outil Form de Fusion_360, ce dernier sera mit à jour lors de l’utilisation un peu plus « intensive » de la machine. 

