# 1. Mise en route


## Découverte de la plateforme

Durant cette première semaine j’ai suivi le module afin d’apprendre les bases de Github afin de construire un site afin d’y développer mon projet et de le rendre appropriable à tous. 

Possédant un Windows, j’ai choisi dans un premier temps de travailler seulement via le site le temps de m’identifier au code du Markdown et de comprendre les rouages du code. Une fois ces derniers « maitrisés » (du moins quand j’aurai compris quelques points…) j’essaierai alors via la modification directe. 

Actuellement quelques problèmes persistent encore après l’encodage des lignes actuelles : Des questions de mise en page assez contraignantes : impossible de centrer une image ou écrire à côté. D’autres problèmes sont également intervenus comme l’impossibilité d’installer le pluggin permettant de visualiser en direct les modifications de code effectuées sur Atom.
 
# Etapes effectuée 

Durant cette première phase plusieurs étapes ont été effectuées afin de s’habituer au site et commencer à se l’approprier. 

- L’ajout d’images dans la plateforme, au niveau du fichier **Docs** et ensuite **image**. En veillant à  respecter les contraintes de taille imposées par le site. Pour ce fait les images ont au préalable été découpés dans leurs résolutions via Photoshop.

- La modification du fichier **mkdocs.yml** afin de modifier le nom du site, de l’auteur (mais également de jouer un peu avec les thèmes et voir si certains pouvaient être intéressant…)

- L’ajout de textes/paragraphes aux différentes parties du site afin de compléter ce dernier, que ce soit dans les **…/docs/modules/module01/md** ou **…/docs/final-project.md**. L’appréhension des règles de code de **Markedown** ont dû être ‘’apprises’’ ( ou du moins partiellement. ) Parmi ces dernières certaines ‘’importantes’’ sont à noter : 

# Base de code utilisé

- «! [ ] ( ../images/nomdel’image. format)» afin d’appliquer une image sur la page. 

- «* * Mot * * » Afin de mettre ce dit mot en **Gras** 

- «[Nom de la référence ] (http ://sitedelaréférence)» afin d'indiquer une référence sur une page 


Evidemment, les espaces sont à retirer afin de créer les liens nécessaire à l’ajout des codes sur la page. 

