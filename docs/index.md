Salut ! Je m’appelle Simon Tirello

Bienvenue sur ce site réalisé sur Gitlab et qui a pour vocation pour présenter mon parcours de A à Z au sein de l’atelier Design & Architecture enseigné en Master à la faculté d’architecture ULB la Cambre Horta durant l’année 2020-2021. 

Ce cours à pour but de nous enseigner les bases du Git afin de l’utiliser comme BIM et à vocation d’échange, mais également de nous intéresser au Design et aux moyens de représentation/modélisations mis à notre disposition par le FabLab. 

Le but sera de faire des allers retours entre apprentissage & expérimentation, tout en archivant au mieux les succès et erreurs afin d’aboutir à la réalisation d’une réinterprétation d’une œuvre d’art. 


## A Propos de moi 

![](images/avatar-photo.jpg)

Je m’appelle Simon Tirello, j’ai 26ans et je suis actuellement en Master 2 d’architecture à la faculté d’Architecture ULB la Cambre Horta. 

## Mon Contexte (littéral)

Je viens de Montpellier, en France, ville dans laquelle j’ai habité pendant presque 20ans avant de venir en Belgique pour mes études. Pour compléter ce BACKGROUND, je suis assez casanier, j’aime le sport, j’ai la main verte, et j’aime particulièrement travaille de manière empirique. 

La semaine passée, la recherche du travail après la visite au musée fut centrée autour de l’objet choisi. En l’occurrence pour mon cas, le choix de cet objet s’est orienté vers un fauteuil, le Nastro de Cesare Léonardi & Franca Stagi. 

## Choix de l'objet - le fauteuil "Nastro" de Cesare Léonardi & Franca Stagi

L’objet que j’ai choisi est la chaise « Nastro » réalisé en 1961 par Cesare Léonardi & Franca Stagi. Il est assez difficile d’expliquer clairement ce choix tant une multitude d’objets me paraissaient intéressants. La différence entre cette chaise et d’autres m’intéressant initialement est la sobriété de cette dernière, ses courbures, et son aspect brillant ne semblant pas être du plastique comparé aux autres l’entourant ( pour le coup c’est de la fibre de verre, donc effectivement. ) 

![](images/ref.jpg) 

Ayant beaucoup travaillé en maquette par le passé et orientant mon mémoire autour de ce moyen de représentation, cette chaise m’a sauté aux yeux comme une représentation concrète de l’expérimentation maquette. On ressent parfaitement l’exercice des designers autour d’une feuille, à la plier pour lui faire prendre une forme ou une autre jusqu’à aboutir à un résultat souhaité. Et c’est ce qui rend aussi délicat la représentation numérique de cet objet, c’est qu’elle est faite par la main, par une torsion et une translation manuelle sur une feuille de papier, et non une création vectorielle comme beaucoup d’autres objets pouvaient être designer dans le musée. 

![](images/vue1.jpg)
![](images/vue2.jpg)

## Travaux Précédents 

Par le passé, un de mes projets me semblait en lien avec l’intitulé de cette option, celui réalisé l’an passé dans l’atelier Digital Fabrication Studio.  

### Project A - DIGITAL_FABRICATION_STUDIO Q2 2019/2020

Mon but au sein de l'atelier de DFS est la découverte et la mise en oeuvre d'architecture cinétique par le biais de structure déployables.

Le projet s'ancre dans un contexte expérimental et utopique d’éléments urbains articulés. 
 

![](images/dfs_im1.jpg)

Le travail s’est fait de manière empirique avant et après le confinement. 

Initialement le but était de concevoir et de réaliser à échelle humaine une structure déployable réalisé au préalable durant la 1ère moitié du semestre en prototype imprimé en 3D. Suite au confinement le travail s’est orienté autour des maquette numérique et l’apport que ces dernières pouvaient apporter au projet. Le logiciel Fusion_360 fut d’une grande aide, que ce soit via les fonctions mécaniques qu’il apporte mais également les vidéos qu’il permet de réaliser, afin d’expliquer au mieux le projet, mais également de « tester » la maquette une fois cette dernière réalisée en 3D.

![](images/cube_décomposé_v2.gif)
![](images/cubes2.gif)

Pour finir, le projet s’est finalement orienté vers un aspect plus « utopique » avec comme référence de l’Archigram ou Superstudio. 

