# Projet Do&ReDo

![](images/mecc.jpg)


Le projet que j’ai réalisé s’oriente autour de deux voies entremêlées : Une première expérimentale tournant autour de la courbure du bois et des procédés pouvant lui offrir une certaine flexion. Et une seconde plus empirique mettant en pratique ces expérimentations via les outils mis à notre disposition. Cette façon de travailler très empirique est constituée de multiples allers retours entre les deux voies suivant les résultats obtenus, d’où le titre du projet : Do&ReDo. 

Le travail se base sur des études et des recherches menées en amont sur les différentes techniques de torsion du bois, notamment celle du Lattice Hinge. Cette technique sera finalement celle employée pour le projet pour sa transparence, sa capacité de torsion et son esthétique qui lui est propre. 

La recherche de forme au niveau de la maquette a également été un point crucial. Cette dernière se devait d’être pragmatique vis-à-vis des résultats possibles. La forme s’est finalement orientée vers une double courbe assez équilibrée, et dessinant une table basse élégante et élancée

- [Lien de téléchargement (Fichiers&Notice)](https://drive.google.com/drive/folders/1IHV2qPZLJob4WdSgaKAE-ytfwjNXJg9F?usp=sharing)

# Recherches

# 1. Etat de l'art, choix de l'objet et documentation

La semaine passée, la recherche du travail après la visite au musée fut centrée autour de l’objet choisi. En l’occurrence pour mon cas, le choix de cet objet s’est orienté vers un fauteuil, le Nastro de Cesare Léonardi & Franca Stagi. 

## Recherches

Réalisé entre 1961 et 1969 à Bernini en Italie, le fauteuil Nastro est le fruit du tandem Léonardi – Stagi. À la suite de la biennale de Venise de 1968 et des œuvres de Max Bill, les deux Designers s’intéressent tout particulièrement aux structures à la forme résistantes, et c’est dans cette lancée qu’ils créent alors le fauteuil Nastro réalisé en fibre de verre, à la fois dossier, siège et accoudoir. C’est cette œuvre, aussi particulière en apparence qu’en performance qui va lancer leurs carrières. Durant ces dernières, ils vont utiliser des manières traditionnelles et artisanales de concevoir leurs réalisations, en utilisant des planches de bois comme coffrage pour le béton par exemple, et cherchent à obtenir des objets d’un seul tenant, sans gaspillage de matière. Durant leurs carrières, plus de trois cents meubles sont créés. 

| Conception de la maquette  | Pli final  |
| --- | --- | 
| ![](images/ref.jpg) | ![](images/unnamed.jpg) |

Pour en revenir sur cette œuvre si particulière, elle ressemble à une feuille de papier simplement aplatie en son centre sur elle-même. Les pieds sont en aciers et viennent jouer avec la forme de l’assisse en créant un porte-à-faux totalement maitrisé et fournissant un degré d’élasticité tout particulier à ce fauteuil.  

![](images/moule.jpg) 

Ici, on se rend compte de comment cet objet a été réalisé : Grace à un moule standard

## Contexte Historique.

D’un point de vue du contexte ambiant au début des années 1960, juste avant la création du Fauteuil Nastro par les deux architectes, il faut dire que le contexte est relativement tendu. 

A la suite de la 2e guerre mondiale, les grandes puissances se sont alors lancées dans une escalade de développement atomique. L’apothéose de ce mouvement arrive en 1961 avec l’essaie atomique de la bombe Tsar, bombe atomique la plus puissante lancée à ce jour. ( les grandes puissances se mettront d’ailleurs d’accord en 1963 pour limite drastiquement les tests nucléaires.) 
Outre le développement exponentiel du risque atomique dans les années 50, 1960 aura également marqué le monde par les tensions présentes entre URSS et Etats Unis, notamment à cause de l’espionnage américain. 

D’un point de vue positif, l’année 1961 amène les premiers vols habités dans l’espace par les russes. La conquête à l’espace est lancée. Du côté des italiens, l’année 1960 marque la présence des jeux olympique à Rome. 

On ressent que si les choses semblaient très carrés et tendus dans les années 1950, elles semblent se détendre un peu dans la décennie suivante. Un relâchement des tensions mondiales et une chute de quelques minutes à l’horloge de la fin du monde, une volonté de marquer le monde d’une façon positive avec l’espace etc… La liberté semble mieux s’exporter, et potentiellement avec elle les rondeur et utopies du design, en l’occurrence ici de la forme et le pliage. 


## Autres productions des auteurs 

| Side Table  | Kappa Table  |
| --- | --- | 
| ![weke](images/side_table.jpg) | ![weke](images/kapa_table.jpg) |

Outre la « Nastro » ou « CL9 Ribbon », le duo de créateur a également œuvré sur une multitude d’autres créations aux origines plus ou moins similaires. Les deux rappelant le plus le fauteuil (outre la chaise à bascule présente également au musée) sont ces deux tables, qui présentent des courbures pouvant être modelés en « origami » comme le fauteuil choisi. 

| Image tirée de la Side Table | Producteur  |
| --- | --- | 
| ![weke](images/FIARM.jpg) | La découverte de cette 1ère table nous informe d’un point relativement important. Jusqu’à lors je pensais que les deux designers produisaient de manière « autonomes » leurs œuvres. Mais visiblement ces derniers avaient bien une marque qui les produisaient : FIARM. Et après vérification, il s’avérerait que les CL9 Ribbon viennent également de cette dernière. |

Après recherches, Fiarm semble être une marque italienne produisant du mobilier « design » dans les années 1960-1970. La marque semble avoir disparue depuis et ne se retrouve sur des sites spécialisé en revente d’objet design.  

Il semblerait que le fauteuil « Nastro » ou CL9 Ribbon fut également confectionné par Bernini ( Sources à vérifier…). Cependant, la marque à l’image de Firm semble avoir disparue ne laissant pas grand-chose derrière elle si ce n’est quelques pièces à vendre sur divers sites de revente 

# 2. Réinterprétation



## 1. Phase de réflexion (1er temps)


Dans un second temps post-analyse, il a donc été question de se réapproprier l’objet afin de le réinterpréter selon notre volonté. 
L’élément que je voulais m’approprier depuis le départ avec cet objet est son mouvement, sa particularité qui a mes yeux le démarquait des autres : ses courbes faites à la main. 

Pour ce fait j’ai donc travaillé sur deux angles d’attaque bien différents : 
- Un premier manuel, avec des bandelettes de papiers comme l’avait fait les auteurs de la Nastro. 
- Et en parallèle via Fusion 360 et l’outil Form mit à notre disposition par ce dernier. 

Le fait de travailler à la main a permit de voir apparaitre de premières formes (même si je n’ai ici pas de photo étant donné que le papier n’arrêtait pas de reprendre sa forme initiale). Le passage aux outils 3D ont réellement permit de franchement un cap dans la facilité de mise en œuvre. 

Dès le début j’ai souhaité réinterpréter l’objet sous une forme différente de celle qu’il avait initialement. 

En l’occurrence, mon choix s’est porté sur une table basse. Plusieurs raisons m’ont poussé à prendre cette décision : 
- La structure portante d’une assise nécessite une certaine stabilité. Dans la chaise Nastro, cette dernière est relativement élancée, fine, et se place presque en retrait vis-à-vis de l’assise afin de mettre en lumière cette dernière. Avec les outils mis à notre disposition au fablab il aurait été très délicat de trouver le juste milieu entre structure portante, et structure légère. Dans une temporalité autre, que ce soit or Covid ou avec plus de temps il aurait été potentiellement possible d’y arriver, mais dans ce cas de figure c’était un choix très risqué. 
- J’hésitais à la base entre une bibliothèque ou la table basse. Mon choix s’est finalement porté sur la seconde tout simplement car la bibliothèque à mes yeux nécessitait réellement des surfaces « planes et plates » alors que la table basse laissait plus de libertés aux courbes. 

## 2. Phase de modélisation 3D. 


À la suite du premier travail, la modélisation s’est poursuivit sur Fusion jusqu’à arriver à un premier jet de structure. 

![](images/fusiontable.jpg)

La structure garde bien les code préétablis par le fauteuil initial Nastro que sont l’aspect malléable de cette dernière, mais également le ruban cylindrique du premier pattern. Durant mes tests plusieurs styles de tracés de bases ont été envisagés : la bandelette, le carré, le triangle etc… Pour finalement revenir sur le cylindre, qui semble être relativement agréable à manipuler et qui garde relativement bien son charme une fois transformé. 

![](images/blanct.png)

Ici une vue de la structure en transparence, laissant bien apparaitre ses courbes. La question à ce stade là était donc de s’intéresser à la matière de la structure. 

En l’occurrence, pour être relativement fidèle à l’œuvre d’origine, il serait préférable que cette dernière soit donc en fibre de verre. Des premiers tests ont alors été réalisé par la suite.  

![](images/blanc2.png)

![](images/blanc1.png)

La couleur blanche marche assez bien avec l’objet initial (même si très saturé et chaleureuse pour les images si dessus.) 

Ayant modélisé lors de l’apprentissage de la fonction Form le fauteuil Nastro ( vous en trouverez un tuto dans l’onglet Module 2), j’en ai profité pour faire un rendu comparatif des deux modèles. 

![](images/COMPARATIF.jpg)

On ressent bien les courbes initiales du fauteuil et les deux objets semblent avoir le même vocabulaire, ils marchent assez bien ensembles. 

A la suite de ce premier essai plutôt concluant au niveau de la forme, je trouvais cependant que la couleur ne mettait pas assez en avant les courbes présentes dans la réinterprétation. Plusieurs essais ont été effectuée et mon dévolu s’est porté sur le rouge, mettant davantage en avant les ombres.

![](images/doublerouge.jpg)

Comme on le voit ici, les ombres et courbes sont  mieux mises en valeur.

![](images/rougep.png)

Un questionnement sur la mise en œuvre avec les outils du fablab a alors été envisagé. La machine en elle-même n’est pas réellement choisie et définie. Cependant, si l’objet réinterprété reste comme il est, et que sa forme reste en simili « 2D », alors j’ai assez vite opté pour une découpe par tranche de l’objet. Permettant même alors de l’agrandir ou le rétrécir sur sa largeur lors de sa création. 

La méthode ne tiendrait alors pas compte de l’épaisseur des matériaux mais seulement de leurs surfaces. Un carton alvéolé d’un bon centimètre pourrait très bien être utilisé avec une découpeuse laser, tout comme du bois pourrait être choisi pour la CNC.  

![](images/jaune.jpg)

Ci-dessus une vue de l’objet par tranche. Ici en jaune car pour tester divers couleurs… 

**Il s’est avéré suite à la correction que le but de l’exercice n’était pas de réinterpréter l’objet mais d’en faire une « mise à jour » et de voir ce que serait cet objet si ce dernier était réalisé en 2020.** 

Il n’était donc plus question de changer le caractère de l’objet en le transposant d’un fauteuil en une table ou autre mais de bien garder son état d’origine et de voir comment ce dernier serait produit et avec quel matériau. 

# 3. Remise à niveau de l'objet. 

## 1. Matérialité 

Avant de songer à ne serait-ce que modifier la forme du fauteuil afin de le rendre « faisable » avec les outils qui sont à notre disposition, il serait intéressant de s’intéresser aux matériaux possiblement exploitables, et surtout pouvant être plier d’une quelconque façon, élément primordial du fauteuil Nastro. 

A la suite de la correction de la semaine passée, deux éléments étaient alors ressortis du lot et possiblement exploitables : 
- **le multiplex pliable**
- La méthode du **Lattice hinge**

La première requiert un type de matériaux particulier et la seconde est une technique de ceintrage qui vient affaiblir le matériau pour qu’il se courbe dans la direction souhaitée. 

![](images/methode.jpg)

*Ci-dessus : Exemples d’applications de la méthode du Lattice Hinge afin de créer des courbes.*

Après recherches, il s’avèrerait que le multiplex « pliable » soit des multiplex d’une épaisseur de seulement quelques millimètres afin de lui donner la propriété adéquate. Il ne semble pas que ce dernier soit disponible dans des dimensions autres. Dans ce cas là la méthode la plus évidente pour le courber serait donc le **Cintrage par lamellé/collé**, méthode très efficace mais également très délicate à mettre en œuvre et bien plus couteuse. 

Dans le cas de notre fauteuil, aucune des deux (ou trois) techniques ne pourraient fonctionner. La Première du Lattice Hinge risque de beaucoup trop fragiliser le fauteuil. Surtout si ce dernier est fait de bois. 

La seconde avec le cintrage par lamellé/collé, outre la difficulté évidente de cette dernière semble très peu réalisable et malheureusement ne mettrait pas en avant d’outil à notre disposition. 

Le multiplexe pliable lui ne semble pas être optimal, que ce soit par ses dimensions ou par son coût. Chacune des planches se trouvant environ à une 30e d’euros. A la vue de leurs épaisseurs une quantité certaine serait nécessaire. 

La question de la matérialité semble être une « mauvaise » 1ere question. Etant donné la crise sanitaire et donc la faible quantité de ressources et outils à notre disposition, se centrer sur les outils nous étant accessibles semble être une meilleure approche. 

## 2. Machines à notre disposition.

1. **Lasercut**

Parmi les machines pouvant convenir à une échelle 1/1, le Lasercut parait être un bon compromis afin de faire des premiers tests en vue d’une découpe CNC si nécessaire. 

L’avantage de cette dernière est que même si elle ne peut découper « que » en 2D, cette dernière permet néanmoins de venir travailler par **states**, un peu comme je voulais le faire dans la réinterprétation en table basse. Cette méthode permet en plus d’assurer un minimum de stabilité et une structure potentiellement correcte si la découpe se fait dans des cartons ondulés d’une épaisseur cohérente et dans le bon sens de découpe. 

Cependant, la mise en œuvre d’une découpe de la sorte dans la vue de tests de prototypage est relativement contraignant car chronophages à mettre en œuvre. Ces découpages de « tests » à échelle donc réduites nécessitent de calibrer totalement le Fusion_360 à l’échelle adéquate et faire des states de la bonne épaisseur à cette même échelle. Ce qui signifie que si l’on souhaite changer l’échelle du prototype il sera nécessaire de recommencer tous les calculs préalables à la découpe et refaire de A à Z tout le fichier de découpe.

2. **CNC**

La seconde machine permettant d’arriver à des résultats à échelle 1/1 est sans équivoque la CNC. Cette dernière permet outre le fait de créer des pièces d’une taille d’utiliser des matériaux solides plus que nécessaire pour la création d’un fauteuil. 

La difficulté réside dans le fait que je ne connais absolument pas la machine et qu’hormis par states, pour l’instant je ne vois absolument pas comment cette dernière pourrait m’aider dans la réalisation d’un objet 3D comme un fauteuil.

3. **Shaper Origin** 

Même si la machine présente un gros potentiel ( *Un tuto se trouve dans le module 5* [ICI](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-design/simon.tirello/modules/module05/) ) l’utilisation de cette dernière pour générer des courbes dans des dimensions aussi grandes ne semble très clairement pas envisageable. 

4. **Impression 3D** 

Même remarque que pour la Shaper. Si ce n’est que c’est un bon outil de prototypage. Ne prenant néanmoins pas en compte le saut d’échelle jusqu’au 1/1 et la conception qui change forcement du tout au tout. 

On se rend alors vite compte que les machines à notre disposition pour produire in fine du  1/1 sont assez limitées et s’organisent autour de la **CNC** et le **Lasercut**. Pour le reste les machines pourront davantage aider lors de prototypages. 

Cependant, comme vu durant les dernières années lors de projets réalisés en Digital Studio Fabrication, la grande difficulté et le grand inconvénient des prototypages résulte dans le saut d’échelle. Une fois la volonté de changer d’échelle que ce soit du 1/10 ou moins au 1/1, le fait de changer de matérialité (car on ne va pas imprimer la chaise en impression 3D et donc en PLA) ou changer de façon de faire remet tout en question nécessitant une seconde étape d’esquisse pour cette seconde échelle.
 
Il est donc nécessaire de réfléchir au préalable aux outils employables pour la réalisation finale en amont et ne pas subir certains choix plus tard lors du saut d’échelle. 

## 3. Mise en contexte actuel.

Si la question de la matérialité semble encore complexe et celle du procédé de création plus ou moins approchée, il est maintenant temps de se concentrer sur l’objet en lui-même. 

Si l’objet initial semble à mes yeux être encore d’actualité et ne semble pas avoir sensiblement « vieillit », il se pourrait néanmoins qu’on puisse le réajuster dans le contexte actuel. 

1. Un **« green » contexte**

S’il y a bien un aspect qui n’était clairement pas pris en compte dans la décennie de 1960, c’est l’empreinte écologique des réalisations. Il est plus que vraisemblable que si le fauteuil Nastro devait être réalisée en 2020 elle le serait en matériaux recyclables comme du **plastique recyclé par exemple**

![](images/nastrobio.jpg)

*Ici un collage réalisé par mes soins représentant le fauteuil Nastro réalisé à partir de billes de plastique recyclé.*

Que ce soit en plastique recyclé ou en **matériaux moins polluant** qu’utilisés initialement, il y a fort à parier que la réalisation de la pièce prenne une tournure différente.

Néanmoins, si les matériaux employés pour sa réalisation pourraient sensiblement changer, **sa forme elle ne devrait pas spécifiquement évoluer**. 

2. Un **« Covid » contexte**

Il est inutile d’argumenter durant des heures pour voir que la Covid apporte avec lui toute une nouvelle série d’approches. Que ce soit préventive ou programmatique. 

Hypothétiquement, râce ou à cause de la distanciation sociale, le design de certains objets pourrait évoluer et être remis en causes afin de prendre en compte cette nouvelle dimension. 

Outre les barrières de plexiglas qui encadrent désormais les lieux publics, je parle ici du 1m50 de « bulle » que toute personne doit respecter. Il est donc possible que les créations futures prennent en compte cette bulle lors de leurs conceptions. Je ne parle pas ici de revenir à des objets de type « œufs » comme réalisés durant la même décennie que la Nastro mais plus vers des objets forçant à respecter ces 1m50. Que ce soit par des dossiers ou de accoudoirs extrêmement épais permettant d’isoler la personne située dans le fauteuil. 

![](images/bigchair.jpg)

*Ici par exemple j’ai simplement repris le design de la chaise initiale avec le pliage qui lui est propre et ajouter une épaisseur bien plus importante afin d’à la fois créer une distanciation sociale claire, et à la fois lier le pied à la chaise.*

Evidement il s’afit ici dans l’exemple qu’une version simple de l’idée. Optimalement il faudrait que la distanciation soit d’au moins 1m50 de part et d’autre ou du moins de 1m et quelque si on prend en compte le fait qu’une personne passant à côté de cette dernière laisse une distance « normale » entre lui et le fauteuil.  

## 4. Une mise en commun des deux contextes comme idée de mise à niveau

1. Idée de base.

Il serait intéressant de voir comment on pourrait lier les deux contextes afin d’obtenir un objet à la fois écologique tout en essayant de l’adapter aux conditions sanitaires du au Covid. 

Il est également bon de tenir compte des remarques faites en amont sur les machines à notre disposition afin de prototyper ou du moins imaginer un **objet faisable.**

- J’ai pour ce fait la volonté d’essayer de **travailler par states** comme voulu dans la première partie du travail. 
- L’objet prendra cependant une **forme** plus **similaire** à celui du **« contexte Covid »** , ce dernier présentant en effet des avantages d’un point de vue structurel mais également contextuel. 
- Contrairement à l’objet dessiné en exemple ce dernier se devra cependant d’avoir une **base plus élancé vers l’arrière** afin de répondre à des questions de **stabilité.** 
- **La matérialité** envisagée dans un premier temps serait du **carton alvéolé** , de préférence recyclé avec une largeur, la plus grande possible. Ce matériau présente également l’avantage d’être **léger et peu couteux**. 
 
![](images/fatchair2.jpg)

*Ici un exemple de ce que pourrait-être cette chaise (**HORS MATERIALITE**).*


# 4. Expérimentations de la courbe.

Dans un 4e temps, il a été nécessaire de faire un aller-retour dans les phases du projet et faire abstraction de tout objet final ou entier. Il est état dans cette partie-ci de faire une ellipse afin de se focaliser sur l’expérimentation afin d’en ressortir des matériaux et techniques. 

Cette étape permettra également de tester certaines machines afin d’apercevoir leurs potentiels et voir comment il serait possible de relier certaines machines entres-elles afin d’envisager par la suite un objet assemblé plus « complet » ou du moins dans une autre échelle. 


## 1. Méthode du **Lattice Hinge.** 


Si cette méthode en tant que telle ne semble pas pouvoir être utilisée seule, elle permet néanmoins couplée à d’autres procédés (apportant une certaine résistance perpendiculaire aux fibres fragilisées par la technique) de créer un objet présentant une allure courbe et légère, tout en étant relativement résistante. 

Pour ce fait j’ai donc choisi d’expérimenter cette technique avec la **Shaper**. 

Dans un premier temps il m’a fallut faire un choix en termes de structure et de comment j’allais retirer de la matière pour fragiliser le bois utilisé. 

![](images/lh.jpg)

Ici on peut voir les deux fichiers que j’ai réalisé dans un premier temps sur Fusion_360 afin de les exporter par la suite en SVG pour qu’ils soient exploitables par la **Shaper**. 

*Il est d’ailleurs bon de noter que pour que ces derniers soient exploitables, il est nécessaire de télécharger au préalable un plugin permettant de passer directement de Fusion_360 à la Shaper. Ce plugin est téléchargeable sur leur site : [ICI]( https://support.shapertools.com/hc/fr-fr/articles/115003071253-Installation-de-l-add-in-Shaper-Utilities-pour-Fusion-360)*

Mes choix se sont orientés autour de deux bases relativement similaires. 
- **La première à gauche** Présentant 3/2/3/2 « raccords » entre les parallèles
- **la seconde** 2/2/2/2 raccords.

À la suite de cela un premier test de découpe a alors été réalisé mardi après-midi. Il reprend le fichier SVG du pattern de gauche. 

![](images/lh3.jpg)

La planche utilisée est une planche de Multiplex 8mm de 43cms de largeur. La fraise utilisée est celle de 8mm avec le réglage qui lui est propre. 
Pour des questions de sécurité (surtout auprès de la shaper) il a fallu entre 2 et 3 passages pour transpercer la pièce de multiplex. La découpe a durée environ 2h. 

La photo de gauche permet de rendre compte de la quantité de dominos plus ou moins nécessaire pour la découpe d’une pièce de cette taille. A noté que sans les dominos au centre de la découpe il m’était impossible de fraiser le bas de la pièce. 

Le résultat est relativement bon et propre (une fois poncé bien évidemment) cependant le **degré d’élasticité n’est vraiment pas grand**. Même si le multiplex a gagné énormément en élasticité, la courbure voulue initialement n’est pas là. 

** L’angle obtenu doit être d’environ 30° en forçant sur les deux côtés.**

Après avoir discuter avec les Fabmanager, l’idée de **plonger dans l’eau durant une temporalité assez longue la planche fragilisée** m’a semblé être bonne et expérimentable. Cependant j’aurai tout de même envie de tenter de créer le second essai avec l’autre pattern afin de plonger les deux planches dans l’eau en même temps afin de concrètement voir laquelle est la plus apte à prendre une courbure et surtout afin de savoir quelle méthode est la meilleure. 

Mercredi matin a donc été consacré à la découpe du second pattern. 

La découpe a pris environ 1h30 et a été plus rapide grâce à l’expérience acquise la veille.

**Cependant** un problème s’est posé auquel je n’avais pas réellement de réponse. J’avais choisi de prendre comme largeur de perforation 8mm afin d’avoir à passer une seule fois et dans un seul sens. Cette technique marchait pour la moitié des tracés mais sans raison la seconde moitié était « introuable ». Pour résoudre cela il a fallu que je ruse en baissant à 7,9mm sur la Shaper le diamètre de la mèche. ( A noté que les deux tracés étaient exactement identiques ) 

![](images/lh4.jpg)

*Sur l’image de droite on voit d’ailleurs sur l’écran le diamètre à 7,9 comme dit précédemment.*

![](images/lh5.jpg)

Voilà le résultat final avec à gauche le 1er et à droite le 2ème pattern. 

![](images/lh6.jpg)

On se rend très vite compte en comparant les deux que le pattern n°2 présente bien plus de souplesse et de possibilités. 

A droite on peut observer que même **sans contrainte si ce n’est son poids propre,** là où le premier a du mal à se tordre et reste plus ou moins dans sa position initiale, **le 2 plie et attend déjà quasiment 40°**. (là ou le 1 n’atteint guère quelques degrés. 

![](images/lh7.jpg)

** Avec une contrainte** en l’occurrence ici une torsion de ma part de chaque côté du lattice, ( force toute relative n’ayant clairement pas pour but de casser ni de faire « craquer » le bois, il est important de le spécifier ici.) on se rend compte que si **le 1 atteint 30°** comme dit précédemment, **le 2 atteint lui 90° !** et peut commencer à donner quelque chose pour la suite. 

A la suite de cela, **les deux planches perforées ont alors été déposées au fond d’une bassine d’eau**, et ce le mercredi vers 12h. 

![](images/lh8.jpg)

Ici la photo a été prise vers 17h afin de vérifier si le procédé fonctionne et si le multiplex n’aurait pas tendance à gonfler. Le multiplex ne gonfle pas (pour l’instant) et le procédé semble fonctionner. Je n’ai pas essayé de trop forcer mais les 90° de la seconde planche semblent plus facilement atteignables. 

La suite sera donc d’y retourner dans la journée de jeudi afin de potentiellement (si tout se passe bien, que ce multiplex n’ait pas gonflé et que personne n’y ait touché) venir sortir les planches de l’eau afin de les contraindre avant de les faire sécher afin d’espérer leurs donner une forme courbe une fois sèches. 

Il est bon de noter également que si la Shaper a été d’une grande aide, elle reste néanmoins longue à utiliser et « imprécise » dans le sens où 8mm semble être le minimum que je puisse exploiter. Dans ce sens il ne me sera possible que d’aller vers davantage d’extrusion de matière.
 
Si les tests s’avèrent concluant et que la méthode fonctionne, il serait intéressant de décliner cette dernière et potentiellement passer à la CNC. 

**Après plusieurs jours à sécher** le résultat s’avère concluant dans les deux cas :
- Dans la premier cas le bois conserve ses 90° pris lors du mouillage. 
- Dans le second cas, ce dernier garde très largement les 180° et les deux extrémités se touchent très facilement avec une toute petite force.

Il est bon de savoir qu’une fois la courbure prise, le bois ne reprend pas sa rigidité initiale et peut dans les deux cas un peu se modeler. Cela dit je pense que cela reste fragile et une trop grosse force à l’inverse du pliage réalisé risque de casser la structure ( en essayer de le remettre à plat par exemple ). 

![](images/fin2.jpg)


![](images/fin1.jpg)

Ici en haut le 1er pattern après séchage et en dessous le second.

## 2. Test de torsions. 

À la suite de ce premier essai plus de concluant, plusieurs questions se sont alors posées : 
- La question de **la torsion** du bois, afin de voir si cette dernière est possible. Le Lattice Hinge nécessitant (en général) de se plier à la contrainte du sens des fibres du bois. 
- Des essais de **juxtaposition** de plusieurs patterns afin de commencer à entrevoir des formes « complètes ». 

**Deux fichiers sont alors réalisés** : 
- Un premier pour la **Shaper** afin de tester des **torsions** dans **deux sens simultanément.**
- Un second pour la **Découpe laser** à échelle plus réduite ayant pour but de tester plusieurs découpes sur une même longue bande afin que cette dernière se referme sur elle-même un **un peu à l’image de la chaise Nastro**. Cette découpe présentera également un **nouveau pattern** central présentant un motif triangulé. C’est un test ayant pour but d’entrevoir une **souplesse sur plusieurs axes.** 

Le but ici avec ce seconde fichier est de découper dans du 3mm à petite échelle avant de passer sur du 8mm demandant beaucoup plus de temps de découpe ( et de matériel. ) 

![](images/shaper02.jpg)

![](images/decoupelaser.jpg)

*De haut en bas : Fichier pour la Shaper/Fichier pour la laser.*

Le premier fichier traduit en réalisation fut celui de la laser et malheureusement fut un échec.
La laser a en effet eu énormément de mal à découper la triangulation et fumait énormément. Cela dit la découpe était relativement propre si ce n’est que le laser n’allait pas partout à 100% au travers. 

Des prochains essais seront réalisés en séparant la triangulation et le reste du tracé afin d’avoir dans un cas au moins un résultat favorable. 
![](images/test10.jpg)

*On voit bien ici que le laser a beaucoup trop chauffé sur les triangles.*

Dans un second temps c’est le fichier Shaper qui a été utilisé. 

Pour ce fait j’ai acheté un nouveau panneau de multiplex cette fois-ci de 61cm de large (au lieu de 43) afin d’agrandir les bandelettes de découpes pour dans le futur caller plusieurs patterns sur une même bande. 

Si la découpe s’est de nouveau déroulée sans encombre, il s’est avéré que le **multiplex est de très mauvaise qualité**. En effet, les panneaux semblent très mal collés les uns sur les autres. Et dans un projet comme le mien avec des pliages et torsions, le fait de mettre de la **tension sur des lamellés mal collé entraine de la casse.**

Il s’est finalement avéré que seulement une moitié de la structure réalisée était exploitable. L’autre étant trop fragilisée par la faible qualité du bois. 

![](images/qualiténule.jpg)

On voit même sur la photo ci-dessus qu’il manquait même du bois par endroit… Créant ainsi de véritables « porte à faux » dans la structure déjà mise à mal par mes soins. 

Une partie de la structure a donc été mouillée (la moitié à cause de la casse…) et mise en tension/torsion à sécher. 

![](images/torsion.jpg)

La méthode de lattice utilisée est celle qui avait le mieux réussie dans la première vague de test, et la structure **vrillée** se replie sur elle-même à 180°. La faible qualité du bois ne me permettant pas de réaliser un « vrai » changement de plan en lui faisant prendre un nouvel axe. Cependant une fois la tension retirée une fois sèche, il est possible que le changement d’axe s’opère de lui-même. 

# 5. Alternance des méthodes et création de l’objet. 

À la suite de ces diverses expérimentations, le temps du concret est alors arrivé. 
Il m’a donc fallut faire de multiples allers-retours entre ce que j’étais capable de créer comme mécanique avec les méthodes employées jusqu’à présent et des bandes de papier que je tordais dans un peu tous les sens à la recherche d’une forme adéquate et belle. 

Je suis alors arrivé à un objet relativement « faisable » et présentant de bonnes caractéristiques. 

![](images/schemas.jpg)

D’un point de vue structurel l’objet présente plusieurs points d’ancrage au sol rendant sa structure autoportante et viable.  

Il est fabriqué à partir d’une bande comme utilisé jusqu’à présent avec la Shaper et est une alternance de lattice Hinge droit et oblique dans plusieurs sens. 

Une première tentative de réalisation a alors été entreprise dans la semaine du 07/12. 

![](images/r1.jpg)

Cependant, l’objet s’est « cassé » lors de sa mise en eau. Les raisons sont multiples mais très probablement humaines. J’ai très certainement trop « forcé » afin de faire rentrer l’entièrement de la structure dans ma baignoire. 

Cela dit, le bois utilisé (5,5mm) étant nouveau, il est possible qu’il ne soit également pas étranger à la casse… 

![](images/r2.jpg)

Une seconde tentative (même 2) a alors été réalisée la semaine d’après. 

Le patron est un peu différent afin que les torsions aient plus de « jeux » lors de leurs mises en place. 

La découpe s’est faite deux fois, une sur un multiplex 3,2mm et une autre fois sur un 5,5mm afin d’avoir (on l’espère) au moins un résultat favorable 

A la suite de ces découpes, l’une d’entre-elles fut mise en eau afin d’être « tordu » pour le pré jury du lendemain. 

Sur le 3,2mm , malheureusement, visiblement à la suite d’une erreur de trajectoire des « ponts » n’inclinant ainsi pas suffisamment le dessus de la table, j’ai alors forcé un peu trop sur les torsions afin de leurs faire prendre une certaine direction et ces derniers ont rompus. 

Sur le 5,5mm qui aurait certainement été plus robuste, une erreur humaine a été la cause de sa casse. 

![](images/sa.jpg)

Cependant, les fibres en pliage simple ( pied de la table ) ont bien tenu et prennent parfaitement les 90° imposés. 

![](images/sz.jpg)

Dans un autre temps, les deux résidus de planches sont encore en eau afin de tester les torsions davantage et trouver d’autres défauts avant les prochaines découpes. 

On peut néanmoins remarqué (sans avoir été plongé dans l’eau, sur des 3,2mm) que la torsion est réussie avant la prise de mémoire de forme qui pose jusque là problème. ( La taille de la baignoire y est aussi pour beaucoup… ) 

![](images/se.jpg)

Maintenant vient l’étape d’après : celle du rebond quant à la structure et le retour à la maquette papier. 
Le but ici est de trouvé les solutions aux petits problèmes affrontés jusque là et essayer de trouver une forme plus structurelle à l’objet final. 

*Ci-dessous : divers tests papier.*

![](images/sr.jpg)

![](images/st.jpg)

![](images/sy.jpg)

# 6. Approche finale.

A ce stade-là, il a alors fallu être pragmatique. La torsion avec les lattices fonctionne mais seulement lorsqu’elle est dans le sens des fibres. Or si je souhaite faire un objet à base d’une seule bande, je ne pourrai pas donc appliquer plusieurs torsions et flexions sans aller à l’encontre du sens des fibres. 

Il a donc été nécessaire de réaliser des tests papier de formes « faisables », c’est-à-dire soit présentant un seul type de mouvement : des flexions, ou alors un objet composé de plusieurs bandes présentant une seule torsion chacune.

A la suite de ces diverses expérimentations papiers, une forme est ressortie comme étant davantage réalisable et présentant un aspect relativement esthétique. Cette dernière n’utilise que des flexions et la maquette en papier semble assez bien réagir aux tensions que je peux exercer sur cette dernière. Cette forme dans mon imaginaire représente une table basse et je me la suis approprié comme telle. 

![](images/papierr.jpg)


Le patron de cette même maquette papier a alors été déployé et utilisé afin de voir les zones à plier, et par suite de ce patron un fichier Fusion360 a alors été conçu. 

| Rendu de Fusion de la table  | Patron de la table à découper  |
| --- | --- | 
| ![](images/tablefu.jpg) | ![](images/demip2.jpg) |

Ce dernier se veut comme la représentation exacte du papier en termes de proportion, et ce afin de rester dans la logique de la maquette comme le faisaient les designers de la Nastro. 

Le premier patron a découper reprenait donc les proportions de la maquette papier, et était fait pour une planche de multiplex de 120x60. 

Afin de gagner en temps mais également afin de tester la réaction du multiplex, les découpes ont été réalisés sur du 5 et du 8mm. 

Le 5mm représentant davantage l’échelle puisqu’in fine la table basse serait deux fois plus grande, et donc s’apparenterai davantage à du 10mm. 

![](images/deuxpp.jpg)

Si les prototypes une fois mis en place (après avoir été mouillés pendant une journée) sont plutôt une réussite vis-à-vis du nombre d’échecs par le passé, ils n’en pas restent pas moins « peu » stables. 

Pour lutter contre cette instabilité, des câbles en nylon sont attachés aux lattis de façon symétriques afin de répartir les charges et de mettre en tension les lattis les uns avec les autres lorsqu’une pression est ainsi exercée sur le plateau du prototype. Cela a pour effet d’éviter l’affaissement de cette dernière. 

Ce procédé est une quasi-parfaite réussite, mais sera amélioré lors de l’échelle 1/1 avec des câblages en métal afin d’être plus précis et résistant.

| /  | /  |
| --- | --- | 
| ![](images/protot.jpg) | ![](images/nylon.jpg) |

## Objet final.

 Il a alors été temps de passer au prototype final.

Pour cela mon but était de doubler les dimensions afin d’avoir une table présentant un plateau d’environ 50x40cms, ce qui représente une table basse de taille moyenne. 

Afin de pouvoir passer à une échelle similaire, je nécessitais une bande de près d’environ 240x40. Cela étant impossible a trouver (et encore plus à agencer, manipuler et mouiller) il a donc été nécessaire de « couper » la table en son centre afin d’y faire une liaison entre deux parties distinctes. 

Chacune des deux parties symétriques font alors environ 120x40 et rentrent désormais dans des planches de multiplex standards. 

![](images/demip.jpg) 

Les découpes ont alors été réalisées sur des multiplex de 8mm d’épaisseurs choisis par mes soins (en fonction du sens des fibres et leurs qualités apparentes).

Chacune des découpes prient en moyenne 2h et auraient nécessité d’avantage de temps. Cela dit, étant donné la période et l’unique Shaper à notre disposition et donc le manque de disponibilité de cette dernière, il m’a fallu être rapide et les deux morceaux présentent donc beaucoup d’erreurs qui auraient pu être fatales à la table une fois mise en tension…

![](images/error.jpg) 

Les planches ont alors été tant bien que mal disposés dans ma baignoire (ou plutôt douchette ave petit réceptacle… Ceci n’étant PAS optimal pour mouiller lesdites planches)

Et ces derniers ont alors était disposées afin de sécher dans la position idéale. 

![](images/mep.jpg) 

De la colle à bois a également été utilisée afin de coller les deux planches entre-elles au centre de la table.

La question du câblage est alors venue sur le tapis.
De nombreux câbles ont alors été achetés et mis en place. 

| /  | /  |
| --- | --- | 
| ![](images/cables.jpg) | ![](images/cab.jpg) |

Et les problèmes du saut d’échelle m’ont alors explosés à la figure. 

L’objet était devenu trop lourd pour des rainures de cette taille-là. Les lattices ayant été simplement mis à l’échelle pour le 1/1 et pas retravaillés, même s’ils étaient plus de fonctionnelles par le passé à échelle réduite, sont maintenant beaucoup trop fragilisés par le poids du plateau de la table.  

Les câbles même s’ils permettaient d’obtenir une certaine aide, ne permettaient pas de maintenir la stabilité de la table.

Le saut d’échelle apporte souvent des problèmes, et cela a été le cas ici. A la vue des efforts mis en œuvre pour ce prototype 1/1 et l’investissement effectué en ce dernier, il n’était pas question de s’orienter sur un 2e prototype. Il aurait très certainement fallu des mois en plus pour mettre au point le tracé du lattice idéal pour cette échelle qui n’aurait alors nécessité qu’un câble pour tout maintenir. 

Il a alors fallu faire un choix : Conserver les câbles ou poser des pieds. Soit privilégier l’aspect esthétique et conserver les câbles, soit décider de réellement faire de cette table une vraie table et la faire devenir fonctionnelle. 
Des pieds ont alors été posés, de façon asymétrique par choix personnel.

L’objet a alors été verni, d’abord de façon mate, afin de casser les imperfections et ensuite de façon satin sur les plateaux afin de les rendre fonctionnels et nettoyables. 

![](images/fc.jpg) 

![](images/fc1.jpg) 

![](images/fdet.jpg) 


## Réferences/Ressources

- [Site sur l'architecte](http://www.archivioleonardi.it/it/cesare-leonardi/design/)
- [Ressources](https://www.architetti.com/cesare-leonardi-modena.html)
- [Site sur l'oeuvre globale des architectes](https://www.mostracesareleonardi.it/)
- [Thèse sur le Lattice Hinge très interessante](https://ir.uiowa.edu/cgi/viewcontent.cgi?article=8252&context=etd&fbclid=IwAR23dG1iR3k8xPKllXNZnA4MIlHr_VsDxMk1jArCmVXtqWdYl7Xl2uKSiwM)


## Photos du Fauteuil Nastro prises au musée
![](images/vue1.jpg)
![](images/vue2.jpg)


